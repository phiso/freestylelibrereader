/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package freestylelibre.filereader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class FreestyleLibreFileReader {

    private File srcFile;
    private ArrayList<String> allLines;
    private ArrayList<FreesytleLibreData> dataSets;
    private String ownerName;

    public FreestyleLibreFileReader(File file){
        srcFile = file;
        allLines = new ArrayList<>();
        dataSets = new ArrayList<>();                
        ownerName = "";       
    }
    
    public void read() throws IOException, ParseException{
         if (srcFile.canRead()) {
            readFile();
            dataSets = parseLines();
        } else {
            throw new FileNotFoundException("Cannot read given File: " + srcFile.getAbsolutePath());
        }
    }

    /**
     * reading lines from file, not parsing, just reading raw data
     * @throws FileNotFoundException
     * @throws IOException 
     */
    private void readFile() throws FileNotFoundException, IOException {
        BufferedReader in = new BufferedReader(new FileReader(srcFile.getAbsolutePath()));
        String currentLine = null;
        while ((currentLine = in.readLine()) != null) {
            allLines.add(currentLine);
        }
        // first line contains the name set with the freestyle libre software
        // it is removed extracted because its not relevant for the following data
        ownerName = allLines.remove(0);        
        allLines.remove(0); // the second line only contains headings and is not needed
    }

    /**
     * Splits the given line into its data parts using the schema from files exported by the Freestyle Libre Software
     *
     * @param line
     * @return Returns an array of 18 strings with all data (must be converted)
     * ----------------------------------------------------------------
     *
     * ----------------------------------------------------------------
     */
    private ArrayList<String> splitLine(String line) {
        ArrayList<String> result = new ArrayList<>();
        int tabIndex = -1;
        while ((tabIndex = line.indexOf("\t")) != -1) {
            String temp = line.substring(0, tabIndex);
            line = line.substring(tabIndex + 1, line.length());   // cut the used part of line            
            result.add(temp);
        }
        return result; // length of 18
    }

    /**
     * converts the raw data form all lines to the Java POJO FreestyleLibreData
     * @return returns an array of FreestyleLibreData
     * @throws ParseException 
     */
    private ArrayList<FreesytleLibreData> parseLines() throws ParseException {
        ArrayList<FreesytleLibreData> resultData = new ArrayList<>();
        boolean attach = false;
        int fastInsulin = -1;
        int food = -1;
        int slowInsulin = -1;
        String notes = "";
        int testedGlucose = -1;
        int keton = -1;
        int foodInsulin = -1;
        int correctingInsulin = -1;
        int changedInsulin = -1;

        for (String line : allLines) {
            ArrayList<String> currentLineValues = splitLine(line);
            int id = convertWithCheck(currentLineValues.get(0));
            int type = convertWithCheck(currentLineValues.get(2));
            Date date = convertDate(currentLineValues.get(1));

            if (type > 2 && type != 6) {
                attach = true;
                fastInsulin = fastInsulin == -1 ? convertWithCheck(currentLineValues.get(6)) : fastInsulin;
                food = food == -1 ? convertWithCheck(currentLineValues.get(8)) : food;
                slowInsulin = slowInsulin == -1 ? convertWithCheck(currentLineValues.get(10)) : slowInsulin;
                notes = notes == "" ? currentLineValues.get(11) : notes.concat(currentLineValues.get(11));
                testedGlucose = testedGlucose == -1 ? convertWithCheck(currentLineValues.get(12)) : testedGlucose;
                keton = keton == -1 ? convertWithCheck(currentLineValues.get(13)) : keton;
                foodInsulin = foodInsulin == -1 ? convertWithCheck(currentLineValues.get(14)) : foodInsulin;
                correctingInsulin = correctingInsulin == -1 ? convertWithCheck(currentLineValues.get(15)) : correctingInsulin;
                changedInsulin = changedInsulin == -1 ? convertWithCheck(currentLineValues.get(16)) : changedInsulin;
            } else if (type != 6) {
                FreesytleLibreData data = null;
                int autoGlucose = convertWithCheck(currentLineValues.get(3));
                int manGlucose = convertWithCheck(currentLineValues.get(4));
                if (attach) {
                    attach = false;
                    data = new FreesytleLibreData(id, date, type, autoGlucose, manGlucose, fastInsulin, slowInsulin,
                            food, notes, testedGlucose, keton, foodInsulin, correctingInsulin, changedInsulin);
                } else {
                    data = new FreesytleLibreData(id, date, type, autoGlucose, manGlucose);
                }
                resultData.add(data);
            }
        }
        return resultData;
    }

    private int convertWithCheck(String str) {
        if (!str.equals("")) {
            if (str.contains(",")) {
                return Integer.parseInt(str.substring(0,str.indexOf(",")));
            } else {
                return Integer.parseInt(str);
            }
        } else {
            return -1;
        }
    }

    private Date convertDate(String str) throws ParseException {
        if (!str.equals("")) {
            Date date = new SimpleDateFormat("yyyy.MM.dd HH:mm").parse(str);
            return date;
        } else {
            return null;
        }
    }
    
    /**
     * returns the Name given in the first line of the file.
     * @return 
     */
    public String getFileOwner(){
        return ownerName;
    }
    
    public String getRawLine(int index){
        return allLines.get(index);
    }
    
    public FreesytleLibreData getDataSet(int index){
        return dataSets.get(index);
    }
    
    public ArrayList<FreesytleLibreData> getAllDataSets(){
        return dataSets;
    }        
}
