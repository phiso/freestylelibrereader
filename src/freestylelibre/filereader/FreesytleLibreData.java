/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package freestylelibre.filereader;

import java.util.Date;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class FreesytleLibreData {

    private final int id;
    private final Date date;
    private final int type;
    private final int autoGlucose;
    private final int manGlucose;
    private int fastInsulin;
    private int slowInsulin;
    private int carbohydrates;

    // ------------------------- currently not used --------------------------------------------------------
    private String notes;
    private final int testedGlucose;
    private final int ketone;
    private int eatingInsulin;
    private int correctionInsulin;
    private int insulinChange;   
    // -----------------------------------------------------------------------------------------------------

    public FreesytleLibreData(int id, Date date, int type, int autoGlucose, int manGlucose, int fastInsulin,
            int slowInsulin, int carbohydrates) {
        this(id, date, type, autoGlucose, manGlucose, fastInsulin, slowInsulin, carbohydrates, "", -1, -1, -1, -1, -1);
    }

    public FreesytleLibreData(int id, Date date, int type, int autoGlucose, int manGlucose) {
        this(id, date, type, autoGlucose, manGlucose, -1, -1, -1, "", -1, -1, -1, -1, -1);
    }

    public FreesytleLibreData(int id, Date date, int type, int autoGlucose, int manGlucose, int fastInsulin,
            int slowInsulin, int carbohydrates, String notes, int testedGlucose, int ketone, int eatingInsulin,
            int correctionInsulin, int insulinChange) {
        this.id = id;
        this.date = date;
        this.type = type;
        this.autoGlucose = autoGlucose;
        this.manGlucose = manGlucose;
        this.fastInsulin = fastInsulin;
        this.slowInsulin = slowInsulin;
        this.carbohydrates = carbohydrates;
        this.notes = notes;
        this.testedGlucose = testedGlucose;
        this.ketone = ketone;
        this.eatingInsulin = eatingInsulin;
        this.correctionInsulin = correctionInsulin;
        this.insulinChange = insulinChange;        
    }

    public int getGlucose() {
        switch (type) {
            case 0:
                return this.autoGlucose;
            case 1:
                return this.manGlucose;
            default:
                return -1;
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Getter/Setter">
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @return the autoGlucose
     */
    public int getAutoGlucose() {
        return autoGlucose;
    }

    /**
     * @return the manGlucose
     */
    public int getManGlucose() {
        return manGlucose;
    }

    /**
     * @return the fastInsulin
     */
    public int getFastInsulin() {
        return fastInsulin;
    }

    /**
     * @param fastInsulin the fastInsulin to set
     */
    public void setFastInsulin(int fastInsulin) {
        this.fastInsulin = fastInsulin;
    }

    /**
     * @return the slowInsulin
     */
    public int getSlowInsulin() {
        return slowInsulin;
    }

    /**
     * @param slowInsulin the slowInsulin to set
     */
    public void setSlowInsulin(int slowInsulin) {
        this.slowInsulin = slowInsulin;
    }

    /**
     * @return the carbohydrates
     */
    public int getCarbohydrates() {
        return carbohydrates;
    }

    /**
     * @param carbohydrates the carbohydrates to set
     */
    public void setCarbohydrates(int carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    /**
     * @return the notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @param notes the notes to set
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * @return the testedGlucose
     */
    public int getTestedGlucose() {
        return testedGlucose;
    }

    /**
     * @return the ketone
     */
    public int getKetone() {
        return ketone;
    }

    /**
     * @return the eatingInsulin
     */
    public int getEatingInsulin() {
        return eatingInsulin;
    }

    /**
     * @param eatingInsulin the eatingInsulin to set
     */
    public void setEatingInsulin(int eatingInsulin) {
        this.eatingInsulin = eatingInsulin;
    }

    /**
     * @return the correctionInsulin
     */
    public int getCorrectionInsulin() {
        return correctionInsulin;
    }

    /**
     * @param correctionInsulin the correctionInsulin to set
     */
    public void setCorrectionInsulin(int correctionInsulin) {
        this.correctionInsulin = correctionInsulin;
    }

    /**
     * @return the insulinChange
     */
    public int getInsulinChange() {
        return insulinChange;
    }

    /**
     * @param insulinChange the insulinChange to set
     */
    public void setInsulinChange(int insulinChange) {
        this.insulinChange = insulinChange;
    }   
//</editor-fold>          
}
