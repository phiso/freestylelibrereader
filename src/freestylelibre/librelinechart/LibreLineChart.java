/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package freestylelibre.librelinechart;

import freestylelibre.filereader.FreesytleLibreData;
import java.util.ArrayList;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class LibreLineChart {
    
    private final LineChart lineChart;
    private ArrayList<FreesytleLibreData> srcData;
    private ArrayList<XYChart.Series<String, Integer>> dataSeries;
    private XYChart.Series<String, Integer> upperLimit;
    private XYChart.Series<String, Integer> lowerLimit;
    
    private int lowerLimitValue;
    private int upperLimitValue;
    
    public LibreLineChart(LineChart chart) {
        srcData = new ArrayList<>();
        lineChart = chart;
        dataSeries = new ArrayList<>();
        this.upperLimit = this.lowerLimit = new XYChart.Series<>();
        lowerLimitValue = 70;
        upperLimitValue = 130;
    }    
    
    private void createLimitSeries(){
        
    }
    
    public void addLibreData(ArrayList<FreesytleLibreData> dataList) {
        XYChart.Series<String, Integer> series = new XYChart.Series<>();
        for (FreesytleLibreData data : dataList) {
            XYChart.Data<String, Integer> curData = new XYChart.Data<String, Integer>(data.getDate().toString(),
                    data.getGlucose());            
            curData.setNode(new HoveredThresholdNode(data.getDate().toString() + ": " + data.getGlucose(),
                    data.getId()));
            
            series.getData().add(curData);
        }        
        dataSeries.add(series);
        lineChart.getData().add(series);
    }
    
    public LineChart getLineChart() {
        return lineChart;
    }
    
    public void showLibreData(int index){
        lineChart.getData().setAll(dataSeries.get(index));
    }
}
