/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package freestylelibre;

import freestylelibre.librelinechart.HoveredThresholdNode;
import freestylelibre.filereader.FreesytleLibreData;
import freestylelibre.filereader.FreestyleLibreFileReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;

/**
 *
 * @author Philipp Sommer <phiso08@aol.de>
 */
public class MainFXMLController implements Initializable {

    @FXML
    private AnchorPane parentPane;
    @FXML
    private AnchorPane rightSplitAnchorPane;
    @FXML
    private MenuBar mainMenuBar;
    @FXML
    private SplitPane mainSplitPane;
    @FXML
    private VBox leftSplitVBox;
    @FXML
    private LineChart lineChart;
    @FXML
    private Label rawDataListLabel;
    @FXML
    private ListView rawDataListView;
    @FXML
    private TabPane chartsTabPane;
    @FXML
    private Tab lineChartTab;
    @FXML
    private MenuItem openFileMenuItem;
    @FXML
    private MenuItem closeMenuItem;
    @FXML
    private ScrollPane lineChartScrollPane;

    private XYChart.Series<String, Integer> dataSeries = new XYChart.Series<>();
    private XYChart.Data<String, Integer> data;

    private final XYChart.Series<String, Integer> upperLimit = new XYChart.Series<>();
    private final XYChart.Series<String, Integer> lowerLimit = new XYChart.Series<>();

    private ArrayList<FreesytleLibreData> currentData;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        currentData = new ArrayList<>();
    }

    @FXML
    private void handleOpenFileMenuItem(ActionEvent event) throws IOException, FileNotFoundException, ParseException {
        File file = lookForFile("Textdateien(*.txt)", "*.txt", System.getProperty("user.dir"));
        FreestyleLibreFileReader reader = new FreestyleLibreFileReader(file);
        //currentData = reader.parseLines();
        //fillListView();
    }

    private void fillListView() {
        for (FreesytleLibreData data : currentData) {
            String fastInsulinStr = data.getFastInsulin() != -1 ? "Humalog:" + Integer.toString(data.getFastInsulin())
                    + " Einheiten" : "-";
            String slowInsulinStr = data.getSlowInsulin() != -1 ? "Levimir:" + Integer.toString(data.getSlowInsulin())
                    + " Einheiten" : "-";
            String foodStr = data.getCarbohydrates() != -1 ? Integer.toString(data.getCarbohydrates())
                    + " BE" : "-";
            String text = new SimpleDateFormat("yyy.MM.dd HH:mm").format(data.getDate()) + ": " + data.getGlucose()
                    + ", " + fastInsulinStr + ", " + slowInsulinStr + ", "
                    + foodStr;
            rawDataListView.getItems().add(new Label(text));

            this.data = new XYChart.Data<String, Integer>(data.getDate().toString(), data.getGlucose());
            this.data.setNode(new HoveredThresholdNode(data.getDate().toString() + ": " + data.getGlucose()));
            dataSeries.getData().add(this.data);
        }
        rawDataListLabel.setText(Integer.toString(currentData.size()));
        lineChart.setPrefWidth(currentData.size() * 5);
        lineChart.setPrefHeight(lineChartScrollPane.getPrefHeight());
        upperLimit.getData().addAll(new XYChart.Data<String, Integer>(currentData.get(0).getDate().toString(), 140),
                new XYChart.Data<String, Integer>(currentData.get(currentData.size() - 1).getDate().toString(), 140));
        lowerLimit.getData().addAll(new XYChart.Data<String, Integer>(currentData.get(0).getDate().toString(), 60),
                new XYChart.Data<String, Integer>(currentData.get(currentData.size() - 1).getDate().toString(), 60));
        lineChart.getData().addAll(dataSeries, upperLimit, lowerLimit);
    }

    private void createLineChart() {
    }

    /*
     private ObservableList<XYChart.Data<String, Integer>> plot(ArrayList<FreesytleLibreData> data){
     final ObservableList<XYChart.Data<String, Integer>> dataset = FXCollections.observableArrayList();
     int i = 0;
     while (i < data.size()){
     final XYChart.Data<String, Integer> curData = new XYChart.Data<>(data.get(i).getDate().toString(),data.get(i).getGlucose());
     curData.setNode(
     new HoveredThresholdNode(
     (i == 0) ? 0: data.get(i-1).getGlucose(),
     data.get(i).getGlucose()
     )                   
     );
     dataset.add(curData);
     i++;
     }
     return dataset;
     }*/
    @FXML
    private void closeMenuItem(ActionEvent event) {
        System.exit(0);
    }

    /*
     opens a filechooser at given location to open files of specific extension
     */
    private File lookForFile(String files, String extension, String startdir) {
        FileChooser fc = new FileChooser();
        FileChooser.ExtensionFilter extFilter;
        if (extension.contains("|")) {
            String[] exts = extension.split("|");
            List<String> ext_list = new ArrayList<String>(Arrays.asList(exts));
            extFilter = new FileChooser.ExtensionFilter(files, ext_list);
        } else {
            extFilter = new FileChooser.ExtensionFilter(files, extension);
        }
        fc.getExtensionFilters().add(extFilter);
        File initialDir;
        if (startdir == null) {
            initialDir = new File(System.getProperty("user.dir") + "\\data");
        } else {
            initialDir = new File(startdir);
        }
        if (initialDir.canRead()) {
            fc.setInitialDirectory(initialDir);
        } else {
            fc.setInitialDirectory(new File("c:"));
        }
        File result = fc.showOpenDialog(null);
        return result;
    }
}
